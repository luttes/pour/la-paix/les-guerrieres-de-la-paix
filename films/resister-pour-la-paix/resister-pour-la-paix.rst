
.. _resister_pour_la_paix:

=================================================================================
**Résister pour la paix** film documentaire de Hanna Assouline & Sonia Terrab
=================================================================================

- https://www.youtube.com/watch?v=VMfKKUQzO1A

.. youtube:: VMfKKUQzO1A

2024
=======

- https://www.francetvpro.fr/contenu-de-presse/69442083?auHash=S16aaaVLWEFtVCYEBprW365wWswC1oWS_TI1MpBDyxg
  Résister pour la paix, Dimanche 8 Décembre 2024 à 23.45 sur France 5 et sur france.tv

- :ref:`hanna_assouline_2024_10_09`
