
.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>

.. figure:: images/guerrieres_de_la_paix.png
   :align: center

.. un·e
.. ❤️💛💚

|FluxWeb| `RSS <https://luttes.frama.io/pour/la-paix/les-guerrieres-de-la-paix/rss.xml>`_

.. _guerrieres:
.. _guerrieres_paix:
.. _paix:

===============================================================================
|paix| 💚 **Les guerrières de la Paix** |hanna|
===============================================================================

- https://linktr.ee/lesguerrieresdelapaix
- https://www.instagram.com/guerrieres_paix_mvt/
- https://www.lesguerrieresdelapaix.com/
- https://www.lesguerrieresdelapaix.com/forum-mondial-des-femmes-pour-la-paix/
- https://www.imarabe.org/fr/evenement-exceptionnel/les-guerrieres-de-la-paix-presentent-le-forum-mondial-des-femmes-pour-la-paix

- https://rstockm.github.io/mastowall/?hashtags=paix,peace,pace&server=https://framapiaf.org

.. toctree::
   :maxdepth: 6

   militantes/militantes
   films/films   
   nouvelles/nouvelles
   ressources/ressources
