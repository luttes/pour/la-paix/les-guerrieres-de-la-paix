
.. index::
   pair: Ressources; Guerrières

.. _ressources_guerrieres:

=======================================================================================================
Ressources
=======================================================================================================

- http://linkertree.frama.io/judaisme


#ShalomSalaam #CeaseFireNow #Israel #Gaza #Mazeldon #Jewdiverse #Peace #Palestine
#jewish #ethics #morals #values #betterworld #israel #hebrew

Mastowall
===========

- https://rstockm.github.io/mastowall/?hashtags=paix,peace,pace&server=https://framapiaf.org


|hanna| Hanna Assouline
=========================

- :ref:`hanna_assouline`


|floriane| **Rav Floriane Chinksy**
==========================================

- :ref:`chinsky:floriane_chinsky`


womenwagepeace
=================

- https://www.youtube.com/watch?v=0qRoGPz1JQk&ab_channel=WomenWagePeace-%D7%A0%D7%A9%D7%99%D7%9D%D7%A2%D7%95%D7%A9%D7%95%D7%AA%D7%A9%D7%9C%D7%95%D7%9D
- www.womenwagepeace.org.il/en/
- https://www.womenwagepeace.org.il/en/summary-of-the-activities-of-women-wage-peace-2022/

Contre l'antisémitisme
==========================

- :ref:`antisem:antisem`
