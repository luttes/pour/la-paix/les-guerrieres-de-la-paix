.. index::
   pair: Guerrières de la Paix; 2022-07-11

.. _paix_2022_07_11:

==========================================================================
2023-07-11 **Rencontre @ Mairie de Paris**
==========================================================================


- https://www.lesguerrieresdelapaix.com/actions/rencontre-mairie-de-paris/


Avec @guerrieres_paix_mvt nous avons eu l’honneur de co-organiser une
rencontre avec des activistes palestiniens et israéliens pour la Paix
aux côtés de @allianceformiddleeastpeace Connecting actions et Kaléidoscope
à la @mairiepariscentre.

Un moment fort et émouvant où nous avons eu la chance d’écouter les voix
de celles et ceux qui vivent au cœur de ce conflit et qui œuvrent au
quotidien sur le terrain avec une force et un courage qui forcent
respect et admiration.

Tandis qu’à distance, trop souvent, d’autres soufflent sur les braises
et attisent les haines la parole et l’engagement de ces militants.es
nous obligent et doivent être relayés et mis en lumière.

Merci @huda_dunia @rahamimyuval @ali_abu.awwad @navahefetz 🙏🏼🕊🤍
