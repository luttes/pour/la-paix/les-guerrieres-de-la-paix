.. index::
   ! Nous, les femmes, nous ne pouvons pas consentir à la déshumanisation de nos sœurs d’où qu’elles viennent (2024-03-07)

.. _guerrieres_2024_03_07:

=============================================================================================================================================
2024-03-07 **Nous, les femmes, nous ne pouvons pas consentir à la déshumanisation de nos sœurs d’où qu’elles viennent** |guerrieres|
=============================================================================================================================================

- https://www.lemonde.fr/idees/article/2024/03/07/nous-les-femmes-nous-ne-pouvons-pas-consentir-a-la-deshumanisation-de-nos-s-urs-d-ou-qu-elles-viennent_6220601_3232.html


.. figure:: images/affiche.webp

Introduction
================

Le collectif français pacifiste et antiraciste Les Guerrières de la paix,
fondé en 2022, lance, à l’occasion de la Journée internationale des droits
des femmes du 8 mars 2024, un appel à la solidarité envers les femmes sans égard
à la religion ou aux origines.

Cette Journée internationale des droits des femmes a une saveur particulière.

Nous savons à quel point les guerres et les conflits peuvent fracturer les luttes et
fragiliser des acquis.
Nous, Les Guerrières de la paix, sommes convaincues que les femmes, lorsqu’elles
s’unissent, forment le plus puissant bouclier contre la destruction du monde.

**Elles sont la résistance**.

**Elles sont celles qui tiennent, qui restent debout**.

Nous, Les Guerrières de la paix, sommes un mouvement français pacifiste et
antiraciste réunissant des femmes musulmanes, juives, chrétiennes, athées,
pratiquantes, incroyantes, de différentes origines, de différentes cultures.

Nous nous inscrivons dans la lignée des mouvements pacifistes des femmes
palestiniennes et israéliennes.
Elles-mêmes avaient déjà été inspirées par le mouvement fondé par des femmes
au Liberia en 2003 pour œuvrer à la fin de la guerre civile.

C’est cette chaîne de solidarité internationale de femmes que nous avons
souhaité prolonger lorsque nous avons organisé la première édition,
le 8 mars 2023, du Forum mondial des femmes pour la paix à Essaouira, au
Maroc.

L’événement a rassemblé des militantes du monde entier, dont **Shirin
Ebadi, Prix Nobel de la paix en 2003**, ainsi que des activistes marocaines,
afghanes, syriennes, palestiniennes, israéliennes, ouïgoures, rwandaises…

Le 7 octobre 2023, le monde a basculé Nous avons participé, le 4 octobre
2023, à Jérusalem, à la marche de "l’appel des mères" au côté de
milliers de femmes palestiniennes et israéliennes, militantes pour la paix et
la justice.
Ces femmes, par-delà les murs érigés pour les séparer et leur apprendre à se
haïr, ont donné au monde entier une leçon de sororité, en marchant ensemble,
main dans la main.

**Le 7 octobre 2023, le monde a basculé. Pas nos engagements. Ni nos convictions**
========================================================================================

Ce jour-là, les premières images du massacre furent des images de violence à
l’encontre des femmes. Les corps de femmes israéliennes exhibés, violés, mutilés,
assassinés furent parmi les premières images d’horreur à inonder la Toile.

Les terroristes du Hamas se sont vantés en direct de leurs féminicides.

**Et, aujourd’hui, nous n’osons imaginer ce que doivent subir au quotidien les
femmes encore otages.
Ne pas condamner ces crimes, ne pas les nommer est une faute morale.
Un manque de respect à l’égard de notre féminisme**.

Dès le début des bombardements destructeurs de l’armée israélienne sur
la bande de Gaza, parmi les premières images d’horreur, il y avait aussi
des femmes, des mères et des enfants sans défense sous les décombres.
**Les femmes sont au cœur du drame humanitaire que le gouvernement de Benyamin
Nétanyahou et de ses alliés d’extrême droite font subir à Gaza**.

**Nous nous tenons auprès de nos sœurs palestiniennes** qui payent le lourd
tribut des crimes causés par la guerre, des deuils et de la destruction.

Nous pensons à la douleur qui leur est infligée de devoir quitter leurs maisons,
de voir leurs enfants affamés, à ces mères qui enterrent leurs enfants le
cœur déchiré, à celles qui ont dû accoucher dans des conditions terribles
au milieu du chaos et des bombardements.

**Nous, les femmes, nous ne pouvons pas consentir à la déshumanisation de nos
sœurs, d’où qu’elles viennent**.

**La souffrance des unes ne relativise en rien celle des autres et nous devons
être capables de les reconnaître toutes**.

**Il est important que nous soyons aussi capables de nommer tous les crimes et
d’être dans la solidarité face à l’horreur vécue par nos sœurs israéliennes
et palestiniennes**.

Notre responsabilité à toutes
=================================

En temps de guerre, les femmes sont en première ligne.

Parce qu’elles incarnent la vie, elles sont des cibles à détruire.

Il est donc urgent qu’elles prennent toute leur place à la table des négociations.

C’est avec cette conscience que la résolution 1325 du Conseil de sécurité des
Nations unies a été adoptée en octobre 2000, avec pour objectif d’accroître
la participation des femmes à la prévention et au règlement des conflits,
ainsi qu’à la consolidation de la paix.

Les femmes doivent être entendues, reconnues et impliquées.

D’ailleurs, lorsqu’elles le sont, la paix advient plus rapidement et elle est
plus stable et plus durable.
Veiller au respect des droits des femmes partout est de notre responsabilité
à toutes.

Si une femme est opprimée, où qu’elle soit dans le monde, ce sont nos droits
à toutes qui sont bafoués.
Nous dénonçons ensemble les féminicides et les violences qui s’exercent contre
les femmes, ici et partout.

**Nos empathies, nos indignations ne connaissent ni déterminisme ni assignation**.

Nous dénonçons le traitement inhumain infligé à nos sœurs d’Afghanistan privées
d’éducation, de soins et de droits.

**Nous nous tenons aux côtés des femmes iraniennes** qui, avec un courage inouï,
continuent de défier le pouvoir des mollahs.

**Nous sommes aux côtés de nos sœurs ouïgoures** victimes d’un génocide et de
viols systématiques commis dans les camps chinois.

Nous pensons à **nos sœurs qui vivent des jours terribles en République
démocratique du Congo**, théâtre de massacres de minorités ethniques, de
féminicides et de viols de masse.

**Nous pensons à nos sœurs arméniennes**, aux violences qu’elles ont subies et à
l’exil qui, encore une fois, les frappe…

**Nous pensons à nos sœurs ukrainiennes**, aux violences sexuelles que nombre
d’entre elles ont endurées, à leurs enfants arrachés et déportés en Russie.

**Nous pensons aux opposantes russes** forcées de vivre en exil.

**Nous pensons au chaos humanitaire dont les filles et les femmes sont les premières
victimes au Soudan**.

Et, malheureusement, la liste est encore bien trop longue.

**Nous, Les Guerrières de la paix, continuerons de nous tenir debout, fières
et déterminées, aux côtés de toutes nos sœurs persécutées, partout dans
le monde.
Il y va de notre féminisme. De notre devoir d’humanité**.

**Nous dénonçons toutes ces injustices, toutes ces violences, tous ces crimes**.

**Et ce serait la faillite de notre humanité que de les opposer, de les
hiérarchiser**.

**La solidarité internationale des femmes est une force de résistance, peut-être
le plus beau rempart au chaos du monde**.

**Le féminisme, c’est la justice, l’égalité et la dignité pour toutes**.

C’est le refus de l’assignation et de la division.

**Le féminisme, c’est la paix**.

Premiers signataires
=======================

- Zar Amir Ebrahimi, comédienne et réalisatrice ;
- :ref:`Hanna Assouline <hanna_assouline>`, réalisatrice et fondatrice des Guerrières de la paix ;
- Julie Gayet, comédienne et productrice ;
- Latifa Ibn Ziaten, présidente et fondatrice de l’Association IMAD pour la
  jeunesse et la paix ;
- Eva Illouz, sociologue ;
- Agnès Jaoui, réalisatrice et comédienne ;
- Fatym Layachi, metteuse en scène, chroniqueuse et membre des Guerrières de la paix ;
- Anne-Cécile Mailfert, présidente-fondatrice de la Fondation des femmes ;
- Leïla Slimani, écrivaine ;
- Illana Weizman, essayiste, militante féministe et antiraciste.

