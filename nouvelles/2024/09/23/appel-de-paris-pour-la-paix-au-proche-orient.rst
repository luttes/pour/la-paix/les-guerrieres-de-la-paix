.. index::
   pair: Guerrières de la paix; Appel de Paris pour la paix au proche orient (2024-09-23)

.. _guerrieres_2024_09_24:

============================================================================================================
2024-09-23 **Appel de Paris pour la paix au proche orient** par Les Guerrières de la paix  |guerrieres|
============================================================================================================

- https://www.eventbrite.fr/e/appel-de-paris-pour-la-paix-au-proche-orient-tickets-1008760558987
- https://www.rhr.org.il/eng
- https://www.allmep.org/allmep_member/taghyeer/
- https://www.seedsofpeace.org/
- https://open.spotify.com/show/5CT8QicPO31pe7AX0jA4Wp
- https://www.colline.fr/
- https://www.standing-together.org/en

.. figure:: images/appel.webp

À l'occasion de la Journée internationale de la Paix, instituée par 
l’ONU en 1981 et ayant lieu chaque année le 21 septembre, l’association 
Les Guerrières de la Paix organise le 23 septembre à `La Colline - Théâtre National <https://www.colline.fr/>`_, 
à Paris, une Conférence pour la Paix qui réunira de grandes figures militantes 
palestiniennes et israéliennes engagées pour la paix, la justice et l’égalité telles que :


- Rula Daood & Alon-Lee Green - Directetrice.eur de l'association `Standing Together <https://www.standing-together.org/en>`_
- Maoz Inon - Militant pacifiste
- Aziz Abu Sarah - Militant pacifiste, journaliste et ex-homme politique
- Ali Abu Awwad - Fondateur du mouvement `Taghyeer <https://www.allmep.org/allmep_member/taghyeer/>`_
- Nava Hefetz - Rabbine et membre de `Rabbis for Human Rights <https://www.rhr.org.il/eng>`_
- Amira Mohammed & Ibrahim Abu Ahmad - Créatrice.eur du podcast `Unapologetic - The Third Narrative <https://open.spotify.com/show/5CT8QicPO31pe7AX0jA4Wp>`_
- Tahani Abu Daqqa - Ancienne ministre de la culture et des sports de l'Autorité Palestinienne
- Jonathan Hefetz & Antwan Saca - Directeurs de l'association `Seeds of Peace <https://www.seedsofpeace.org/>`_

Cet événement, qui tient presque du miracle dans le contexte actuel, sera 
l’occasion d’entendre les voix de celles et ceux qui, au cœur de la guerre, 
œuvrent sans relâche, pour construire des ponts entre leurs deux peuples. 

Malgré les deuils et la destruction, malgré la colère et le désespoir, 
ils réussissent envers et contre tout à maintenir le dialogue et à 
défendre la reconnaissance de leur légitimité mutuelle.

**Ces voix sont celles du courage, de la responsabilité, elles méritent le 
soutien de toute la communauté internationationale, car elles sont les 
seuls jalons du futur**.

