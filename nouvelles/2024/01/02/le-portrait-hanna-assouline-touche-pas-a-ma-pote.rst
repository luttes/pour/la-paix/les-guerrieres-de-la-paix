.. index::
   pair: Assouline ; Le portrait Hanna Assouline, touche pas à ma pote** par Clémence Mary (2023-01-02)

.. _assouline_2024_01_02:

==========================================================================================
2024-01-02 **Le portrait Hanna Assouline, touche pas à ma pote** par Clémence Mary
==========================================================================================

- https://www.liberation.fr/portraits/hanna-assouline-touche-pas-a-ma-pote-20240102_HQBTUSHFVRGXPFGOMRFANXLZNE/
- :ref:`hanna_assouline`

Auteure Clémence Mary
==========================

- https://www.liberation.fr/auteur/clemence-mary/

Hanna Assouline, touche pas à ma pote
========================================

Dans les médias et sur le terrain, la documentariste juive d’origine
maghrébine tente, depuis le 7 octobre, de dépasser les assignations à un camp,
en s’inspirant d’un mouvement féministe et pacifiste israélo-palestinien.
par Clémence Mary publié le 2 janvier 2024 à 14h58

"J’arrive dans une minute, j’ai vraiment une tronche de cake en ce
moment !" Casquette en velours vissée sur la tête et miroir en main,
Hanna Assouline pousse un soupir, calée au fond d’un café rétro du
XXe parisien. Dur d’effacer de ses traits la fatigue et le tourbillon de
ces derniers mois. "Depuis le 7 octobre, je n’ai pas pu atterrir…" 

La veille de l’attaque du Hamas, la documentariste, fondatrice du mouvement
féministe des Guerrières de la paix, manifestait en Israël aux côtés
de femmes israéliennes et palestiniennes. 

Deux jours après, la fille de l’autrice :ref:`Brigitte Stora <antisem:brigitte_stora>` et de David Assouline**, 
ex-sénateur socialiste, déboulait sur les plateaux télé pour tenter de 
faire entendre une voix, réconciliatrice, au milieu du vacarme.

Ne pas se laisser enfermer dans un camp, en finir avec les anathèmes et tenter
de se parler au milieu d’un débat miné, ce serait possible ? 

La démarche de la trentenaire, juive d’origine marocaine par son père 
et algérienne par sa mère, fait rêver autant qu’elle laisse songeur. 
A minima, titille la curiosité. 
Sur les réseaux, où se joue aussi son combat, on la vilipende autant 
qu’on la salue. 
Trop bisounours, dépolitisée, privilégiée, déconnectée des réalités… 
Si elle ne la vit pas très bien, sa nouvelle médiatisation la force à 
avoir la peau dure. 
"La communauté juive me reproche de les trahir, et les musulmans d’être 
trop juive pour comprendre les Palestiniens. 

Mais la paix, ce n’est pas la neutralité. 

"Paix et justice vont ensemble", revendique celle qui tisse des liens avec des féministes
antiracistes du monde entier, y compris du 93 en la personne d’Assa
Traoré. Pour Anne-Cécile Mailfert, présidente de la Fondation des femmes,
qui avait convié les Guerrières de la paix à la marche contre les violences
sexuelles du 25 novembre, "Hanna Assouline montre que la paix n’est pas
juste un discours mais une pratique politique. Ça n’a rien de ringard !"

Sa force, la Parisienne fêtarde la tire de la rue. Du terrain qu’elle arpente
depuis l’école de journalisme. Des gens, des rencontres. 

Installée aux Lilas avec ses deux jeunes enfants et son plombier de mari, 
issu d’un milieu juif plus pratiquant, ses racines sont dans le Paris 
populaire, rue de Bagnolet où vivent ses parents. 
Impossible d’y faire trois pas sans croiser ses vieux potes d’origine 
maghrébine, qui saluent l’intermittente galérienne à base de "wesh Hanoukka ça va ?". 
Un surnom "affectif", s’amuse la fan de hip-hop. 

Depuis le 7 octobre, leur reconnaissance à eux, "c’est ce qu’il y a de 
plus précieux", confie-t-elle. Ce jour-là, elle a perdu
une amie, militante pacifiste, dans le kibboutz de Be’eri. 

"Comme pour tous les Juifs, le 7 octobre a fait ressurgir chez moi des 
peurs historiques", au point de la réveiller en sueur la nuit, quand elle 
cauchemarde d’intrusions meurtrières et d’endroits où se réfugier. 

Mais elle rechigne à jouer les victimes et à tomber dans la psychose. 
Retirer sa mezouza, comme lui ont demandé tacitement ses voisins craignant 
pour eux-mêmes ? Elle y a pensé, mais "jamais de la vie", a tranché sa moitié.

C’est les pieds sur terre, rare juive dans un groupe d’amis composite plutôt
musulman, qu’elle s’est construite. Un ancrage dans le quotidien qu’elle a
filmé en Israël et dans les Territoires, où le mouvement Women Wage Peace,
dont les Guerrières de la paix s’inspire, mobilise des femmes des deux
camps. 

"Chez ces femmes, l’engagement pragmatique remplace l’idéologie: va-t-on 
pouvoir se déplacer, manger, vivre demain ? 
Comment se consoler mutuellement de nos deuils respectifs ?" 

Quid du clash entre universalistes et intersectionnelles, que la marche 
féministe du 25 novembre a semblé revivifier ? 

Les deux termes ne s’annulent pas, plaide-t-elle. "L’universalisme est
un combat collectif, qui nécessite la pleine reconnaissance des oppressions
de chacun".

Sa sensibilité va à l’extrême gauche, mais elle a tiré un trait
sur :ref:`Jean-Luc Mélenchon <antisem:melenchon>` qui attise les haines (NDLR, c'est un euphémisme). 

L’islamophobie est insupportable mais cela ne doit pas invisibiliser 
l’antisémitisme, et vice-versa". 

Et d’avouer s’être sentie souvent isolée dans certaines organisations de 
gauche "qui utilisent l’instrumentalisation du combat contre l’antisémitisme 
par l’extrême droite pour abandonner les Juifs, quitte à confondre la 
lutte pour l’émancipation avec la barbarie du Hamas". 

Dans les milieux communautaires, elle n’est pas plus à l’aise, indignée de devoir
parfois se ranger derrière un rideau à la synagogue, elle, qui admire Delphine
Horvilleur, Rosa Parks et Toni Morrisson. 

Sa filiation à elle, ce serait plutôt SOS Racisme, dont son père, figure 
de la Marche des beurs, a toujours été proche. Au point qu’en 2019, elle 
filmait une campagne de l’association visant à faire dialoguer des jeunes 
de communautés différentes.

Avec un grand-père postier, un autre musicien qui n’a pas supporté
l’installation en France, et une grand-mère vendeuse, sa famille a
vécu bon an mal an une intégration nourrie d’espoirs et de douleurs. 

La République chevillée au corps. Scolarité dans le public, vote utile et à
gauche le reste du temps, pas de patrimoine mais des idéaux, des engagements,
et un attachement à la culture juive séfarade, plus qu’à la religion. 

Un héritage et une position sociale qu’elle assume : "Je ne peux pas gommer ce
que je suis. Ma génération ne veut pas baisser la tête, changer de nom.
 
Etre soi-même est la condition de la vraie solidarité." Son père, lui, décrit
une enfant "boule de feu, indépendante et tenace" et voit d’un bon œil
son engagement public, dans un temps qui favorise le repli.

Son goût pour le dialogue n’a pas toujours empêché des amitiés de se
briser, au fil des crises au Proche-Orient dont elle déplore l’importation
en France. 

"Car son discours est trop creux, ça manque de niaque", tacle la
juriste franco-palestinienne Rima Hassan, pour qui la narration du conflit de
Hanna Assouline ne parvient pas à saisir l’origine de l’injustice vécue
par les Palestiniens. Car trop timide, ne dénonçant pas "l’apartheid",
comme le nomme Rima Hassan, "une situation coloniale documentée par des
organisations juridiques internationales dont l’ONU". Un cran plus loin,
le désaccord – elles se sont croisées – porte aussi sur la critique du
sionisme, que Hanna Assouline assimilerait trop facilement à l’antisémitisme,
selon Rima Hassan.

Plus proche du Maroc que d’Israël où vit une partie de sa famille, notamment
dans le sud, la documentariste reste très attachée à l’Etat juif, qui "ne
pourra pas exister sans que l’Etat palestinien le puisse". 

La colonisation est une "trahison de l’idéal sioniste, d’émancipation socialiste", 
tranche celle dont les parents ont accueilli deux ans une amie palestinienne, 
devenue sœur de cœur. 

Une sororité en acte qui lui semble le seul chemin vers "un
accord qui garantisse aux deux peuples, dans le respect du droit international,
la dignité, l’autodétermination et la souveraineté nationale". 

Prochaine étape, traduire la voix de ces femmes sur les scènes parlementaires ou à
l’ONU, sans **compter un projet de film retraçant la crise actuelle du point
de vue des deux communautés**. 

Dormir attendra.

Biographie
=============

1990 Naissance à Paris.

2015 Diplômée de l’Ecole supérieure de journalisme (EFJ).

2018 Film documentaire les Guerrières de la paix.

2020 Film documentaire A notre tour !
