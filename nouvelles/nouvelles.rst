.. index::
   ! Nouvelles

.. _nouvelles:

==========================================================================
**Nouvelles**
==========================================================================

- https://rstockm.github.io/mastowall/?hashtags=paix&server=https://framapiaf.org
- https://rstockm.github.io/mastowall/?hashtags=hassouline&server=https://framapiaf.org

.. toctree::
   :maxdepth: 5

   2024/2024
   2023/2023
   2022/2022
