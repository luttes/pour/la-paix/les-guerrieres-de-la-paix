

.. _paix_2023_06_15:

===========================================================================================================
2023-06-15 **Les Guerrières de la paix présentent le Forum Mondial des Femmes pour la Paix**
===========================================================================================================

- https://www.imarabe.org/fr/evenement-exceptionnel/les-guerrieres-de-la-paix-presentent-le-forum-mondial-des-femmes-pour-la-paix

Préambule
=============

Il y a un an, en mars 2022, à l'Institut du monde arabe, les Guerrières
de la Paix avaient lancé leur mouvement, constitué autour de femmes
musulmanes et juives unies pour porter une voix commune : celle du refus
de l'assignation identitaire, du courage et de l'acceptation de l'autre.

Ce 15 juin 2023, les Guerrières de la Paix font de nouveau halte à l'IMA
pour un appel à la paix, à la justice et à l'égalité.


Texte
========

Un an après le lancement de leur mouvement à l’Institut du monde arabe,
les Guerrières de la Paix rassemblaient à Essaouira, au Maroc, des femmes
d’exception issues de différents pays pour inaugurer un Forum mondial
des Femmes pour la Paix.

Figures d’engagement, représentantes de mouvements pour la paix mais
également symboles de luttes pour l’émancipation, ces femmes palestiniennes,
israéliennes, iraniennes, afghanes, libériennes, sénégalaises, marocaines,
rwandaises... ont lancé un appel à la paix à l’adresse du monde entier.

Ce 15 juin 2023, le Forum mondial des Femmes pour la Paix fait étape à
l’Institut du monde arabe pour un nouvel appel à la Paix, la Justice et
l'Égalite.

Ce rendez-vous réunira une délégation de femmes militantes ouïghours,
ukrainiennes, russes, iraniennes, palestiniennes, israéliennes, etc.

Les intervenantes
====================

- Dilnur Reyhan, femme ouïghour. Présidente de l’Institut Ouïghour d’Europe
- Robi Damelin, israélienne, mère endeuillée. Membre du mouvement israélopalestinien
  The Parent’s circle Families Forum
- Layla Alschekh, palestinienne, mère endeuillée. Membre du mouvement
  israélopalestinien The Parent’s circle Families Forum
- Chahla Chafiq, écrivaine et sociologue exilée iranienne.
  Militante pour les droits humains et la liberté des femmes en Iran
- Zalina Steve, membre de Russie Libertés, opposante au régime russe.
  Militante pour la démocratie et la défense des droits humains en Russie
- Oxana Melnychuk, réfugiée ukrainienne engagée dans l’humanitaire et
  pour la protection des femmes victimes de la guerre en Ukraine
- Ken Bugul, écrivaine sénégalaise. Militante pour les droits des femmes
- Sonia Terrab, journaliste et réalisatrice marocaine. Militante pour
  les droits des femmes. Co-fondatrice du collectif « Hors la loi »
- Huda Abu Arqoub, activiste palestinienne pour la paix et présidente
  de « The Alliance for Middle East Peace »
- **Noa Gur Golan**, militante israélienne pour la paix et la justice,
  principale protagoniste avec Yara Amayra du film Les Guerrières de la Paix
  d'Hanna Assouline, qui a donné son nom au mouvement
- **Yara Amayra**, militante palestinienne pour la paix et la justice,
  principale protagoniste avec Noa Gur Golan du film Les Guerrières de la Paix
- Jessica Mwiza, militante de la mémoire du génocide perpétré contre
  les Tutsis au Rwanda. Afro-féministe



