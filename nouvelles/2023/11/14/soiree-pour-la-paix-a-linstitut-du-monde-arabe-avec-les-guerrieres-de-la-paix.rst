

.. _paix_2023_11_14:

===========================================================================================================
2023-11-14 19h30 **Soirée pour la paix à l’Institut du monde arabe avec les Guerrières de la paix**
===========================================================================================================

- https://www.imarabe.org/fr/evenement-exceptionnel/programme-pour-la-paix

Soirée pour la paix Projections mardi 14 novembre 2023 - 19:30 Auditorium INSTITUT DU MONDE ARABE
======================================================================================================

- https://billetterie.imarabe.org/selection/event/date?productId=10229157183496&lang=fr


.. figure:: images/soiree_paix.png

   https://billetterie.imarabe.org/selection/event/date?productId=10229157183496&lang=fr

L’Institut du monde arabe invite les Guerrières de la paix à nous faire
entendre les voix de femmes #palestiniennes et #israéliennes qui, **envers
et contre tout, militent pour la #paix.**


|hanna| **Annonce par Hanna Assouline**
===========================================

- https://www.instagram.com/p/CzawPJjrJ7k/?utm_source=ig_web_copy_link&igshid=MzRlODBiNWFlZA==

.. figure:: images/affiche.jpg

En juin 2023, l’Institut du monde arabe accueillait le Forum Mondial des
femmes pour la Paix organisé par les @guerrieres_paix_mvt

Une délégation de 12 femmes ouïghours, ukrainiennes, russes, iraniennes,
palestiniennes, israéliennes…, se réunissaient pour un vibrant appel à
la paix, à la justice et à l’égalité.

Le 14 novembre 2023, l’Institut du monde arabe réinvite les Guerrières
de la paix à nous faire entendre les voix de ces femmes qui, envers et
contre tout, militent aujourd’hui encore pour la paix.

**19h30 Projection Les guerrières de la paix, documentaire (52mn), 2018, Hanna Assouline**
---------------------------------------------------------------------------------------------

Projection Les guerrières de la paix, documentaire (52mn), 2018, Hanna Assouline

En 2014, quelques femmes israéliennes et palestiniennes fondaient « Women Wage Peace »,
un mouvement informel créé autour de l’exigence aussi simple qu’audacieuse
de remettre leurs dirigeants à la table des négociations.

Elles sont aujourd’hui plusieurs dizaines de milliers, de tous horizons
politiques et de toutes origines.

Elles sont les guerrières de la paix.

20h30 Projection des témoignages des femmes palestiniennes et israéliennes
---------------------------------------------------------------------------------

Projection des témoignages des femmes palestiniennes et israélienne Huda Abu Arqob,
Layla Alsheikh et Robi Damelin, filmés lors du Forum Mondial des femmes
pour la Paix le 15 juin 2023.

Diffusion de messages de militants pour la paix palestiniens et israéliens,
recueillis en novembre 2023.


Institut du Monde Arabe 1 rue des fossés saint bernard 75005 Paris
========================================================================

- https://www.openstreetmap.org/#map=17/48.85145/2.35102


.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=2.3519432544708256%2C48.84766362383736%2C2.359024286270142%2C48.85053353457769&amp;layer=mapnik"
       style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=18/48.84910/2.35548">Afficher une carte plus grande</a></small>
