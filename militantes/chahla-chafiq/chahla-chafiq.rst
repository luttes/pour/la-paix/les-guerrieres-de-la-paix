.. index::
   ! Chahla Chafiq

.. _chahla_chafiq:

==========================================================================
💚 **Chahla Chafiq** |paix|
==========================================================================

- https://chahlachafiq.com/
- https://fr.wikipedia.org/wiki/Chahla_Chafiq
- https://www.imarabe.org/fr/evenement-exceptionnel/les-guerrieres-de-la-paix-presentent-le-forum-mondial-des-femmes-pour-la-paix


.. figure:: images/chahla-chafiq.png



Biographie Wikipedia
======================

- https://fr.wikipedia.org/wiki/Chahla_Chafiq


Chahla Chafiq, née en 1954, est une sociologue et écrivaine iranienne,
vivant en France.

Biographie
-------------------

Chahla Chafiq est née et a grandi en Iran.
L’arrivée des islamistes au pouvoir, après la révolution de 1979 à
laquelle elle participa activement, la contraint à l’exil.
C’est en France qu’elle trouvera refuge. Elle a choisi le nom de plume
« Chafiq » en l'honneur de sa mère, Chafiqé (« compatissante »).

Ses premiers textes littéraires, des nouvelles en langue persane, sont
publiés en 1989 par des maisons d’édition iraniennes en exil (France, États-Unis).

Une sélection, sur l’exil et le deuil, est traduite en français et
publiée, en 2005, dans le recueil Chemins et brouillard (Ed. Métropolis).
Son premier roman, Demande au miroir, paraît en 2015 (L’Âge d’homme).

Ses essais portent essentiellement sur les causes et conséquences politiques
et sociales de l’idéologisation de l’islam, notamment au regard de la
situation des femmes : La femme et le retour de l’islam.

- L’expérience iranienne (Ed. Le Félin, 1991),
- Femmes sous le voile,  face à la loi islamique (avec Farhad Khosrokhavar, Ed. Le Félin, 1995),
- Le nouvel homme islamiste : la prison politique en Iran (Ed. Le Félin, 2002) ;
- Islam politique, sexe et genre.

À la lumière de l’expérience iranienne (PUF, 2011), Rendez-vous iranien
de Simone de Beauvoir (Ed. iXe, à paraître).

Ce travail de recherche et de réflexion est également développé dans sa
thèse de doctorat couronnée par le Prix Le Monde de la recherche
universitaire en 2010.

Parallèlement à ses activités d’écriture, Chahla Chafiq travaille dans
le domaine des relations interculturelles en France et, en 2003, elle
crée l’ADRIC (Agence de développement des relations interculturelles pour
la citoyenneté) qui mène des formations et des recherches-actions avec
et auprès d’acteurs sociaux.

De 2003 à 2014, elle dirige l’Agence de développement des relations
inter-culturelles pour la citoyenneté (ADRIC), une association loi 1901.

Deux guides de l'ADRIC sont parus sous sa direction: Face aux violences
et aux discriminations : accompagner les femmes issues des immigrations
et Agir pour la laïcité dans un contexte de diversité culturelle.
Des idées reçues à une pratique citoyenne.

Ces guides ont reçu le label « Année européenne du dialogue interculturel »
en 2008.

En 2009, elle obtient un doctorat en sociologie à l'université Paris-Dauphine.

De 2016 à 2019, elle est membre du Haut Conseil à l'égalité entre les
femmes et les hommes de la République française.
C'est son deuxième mandat dans ce conseil.

En 2017, pour l’ensemble de ses activités, elle reçoit les insignes de
Chevalière de la Légion d’honneur. La même année, elle est distinguée par
l’Iranian Women’s Studies Foundation en tant que la « Femme de l’année ».

En 2018, elle reçoit le prix de l’union rationaliste pour ses contributions
au développement de la pensée critique.

La même année, elle devient membre du Parlement des écrivaines francophones,
dont la première édition s'est tenue à Orléans du 26 au 28 septembre 2018.


Laïcité
--------------


Elle place la laïcité comme l'unique voie pour développer les droits des femmes

