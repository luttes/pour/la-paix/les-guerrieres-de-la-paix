.. index::
   ! Sonia Terrab

.. _sonia_terrab:

==========================================================================
💚 **Sonia Terrab** |paix|
==========================================================================


Biographie Wikipedia
======================

- https://fr.wikipedia.org/wiki/Sonia_Terrab


Sonia Terrab (en arabe : صونيا التراب), née en 1986, est une romancière 
et une réalisatrice marocaine. 

Elle a étudié, entre autres, à Sciences Po Paris


Actions en 2024
========================

Co-réalisatrice du film "Résister pour la paix".
