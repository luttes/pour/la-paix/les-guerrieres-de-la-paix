.. index::
   ! Fatym Layachi

.. _fatym_layachi:

==========================================================================
💚 **Fatym Layachi** |paix| |FatymLayachi|
==========================================================================

- https://www.instagram.com/fatym/
- https://fr.wikipedia.org/wiki/Fatym_Layachi

Biographie Wikipedia
======================

- https://fr.wikipedia.org/wiki/Fatym_Layachi


Fatym Layachi née le 24 octobre 1983 à Casablanca est une actrice marocaine.

Biographie
-------------------

Considérée comme une artiste engagée en matière de libertés individuelles, 
elle apparaît en couverture du mensuel masculin marocain Zyriab, paru 
en novembre 2012, avec des vêtements d'homme. 

Membre du collectif Culture libre, créé en février 2012, et en réaction à 
« l’art propre » prôné par les islamistes du PJD au pouvoir au Maroc, 
la comédienne pose en mai 2012 pour une photo choc, allongée à même le sol, 
sur un tas d’ordures dans une décharge publique de Casablanca. 


Actions en 2024
========================

- :ref:`raar_2024:fatym_layachi_2024_11_27_intro`
