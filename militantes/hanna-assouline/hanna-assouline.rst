.. index::
   ! Hanna Assouline

.. _hanna_assouline:
.. _hanna:

==========================================================================
|hanna| 💚 **Hanna Assouline** |guerrieres|
==========================================================================

- https://www.instagram.com/hanna.assoulinee/
- https://www.instagram.com/guerrieres_paix_mvt/
- https://www.lemonde.fr/m-le-mag/article/2023/10/25/qui-est-vraiment-hanna-assouline-la-militante-sans-slogan-ni-drapeau_6196450_4500055.html

Articles/interventions de Hanna Assouline en 2024
==================================================

- :ref:`raar_2024:hanna_assouline_2024_10_09`
- :ref:`wwp:hanna_2024_01_29`
- :ref:`raar_2024:assouline_2024_01_28`
- :ref:`assouline_2024_01_02`
